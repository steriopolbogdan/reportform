from django import forms

from reports.models import Report


class ReportForm(forms.ModelForm):
    icons = ['fas fa-exclamation', 'fas fa-envelope-open-text', 'fas fa-paperclip',
             'fas fa-at', 'fas fa-user', 'fas fa-phone-alt']

    class Meta:
        model = Report
        fields = ['subject', 'message', 'attachment', 'email', 'name', 'phone']
        labels = {
            'subject': 'Subiect'
        }
        widgets = {'subject': forms.TextInput(
            attrs={'placeholder': 'Va rugam introduceti subiectul', 'class': 'col'}),
            'message': forms.Textarea(
                attrs={'placeholder': 'Va rugam introduceti mesajul', 'class': 'col'}),
            'attachment': forms.FileInput(
                attrs={'class': 'col btn', 'style': 'background-color: #ffffff'}),
            'email': forms.EmailInput(
                attrs={'placeholder': 'Va rugam introduceti emailul', 'class': 'col'}),
            'name': forms.TextInput(
                attrs={'placeholder': 'Va rugam introduceti numele daca doriti', 'class': 'col'}),
            'phone': forms.TextInput(
                attrs={'placeholder': 'Va rugam introduceti telefonul daca doriti', 'class': 'col'})
        }
