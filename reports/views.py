import os

from django.conf import settings
from django.core.mail import send_mail, EmailMessage
from django.http import HttpResponse
from django.shortcuts import render

from reports.forms import ReportForm


def home_page(request):
    context = {}
    if request.method == 'POST':
        report_form = ReportForm(request.POST, files=request.FILES)
        if report_form.is_valid():
            report = report_form.save()
            if report.attachment:
                max_filesize = 26214400
                file_size = os.stat(report.attachment.path).st_size
                max_filesize_exceeded = file_size > max_filesize
                email = EmailMessage(
                    report.subject,
                    report.message + f'''
Date contact
email: {report.email}
Nume: {report.name}
Telefon: {report.phone}
Atasament: {'Atasamentul n-a putut fi trimis datorita marimii de ' +
            str(file_size) + ' bytes.' if max_filesize_exceeded else 'Mesajul contine atasament.'}
                ''',
                    'raportare.frauda@gmail.com',
                    ['raportare.frauda@gmail.com'],
                )
                if file_size <= max_filesize:
                    email.attach_file(report.attachment.path)
            else:
                email = EmailMessage(
                    report.subject,
                    report.message + f'''
Date contact
email: {report.email}
Nume: {report.name}
Telefon: {report.phone}
                    ''',
                    'raportare.frauda@gmail.com',
                    ['raportare.frauda@gmail.com'],
                )
            email.send()
            context['report'] = report
    context['form'] = ReportForm()
    return render(request, 'home.html', context)


def my_mail(request):
    subject = "Greetings from Programink"
    msg = "Learn Django at Programink.com"
    to = "raportare.frauda@gmail.com"
    res = send_mail(subject, msg, settings.EMAIL_HOST_USER, [to])
    if res == 1:
        msg = "Mail Sent Successfully."
    else:
        msg = "Mail Sending Failed."
    return HttpResponse(msg)
