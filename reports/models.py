from django.db import models


class Report(models.Model):
    subject = models.CharField(max_length=100)
    message = models.TextField()
    attachment = models.FileField(upload_to='static/report_attachments/', null=True, blank=True)
    email = models.EmailField()
    name = models.CharField(max_length=50, null=True, blank=True)
    phone = models.CharField(max_length=10, null=True, blank=True)
    status = models.CharField(max_length=15, default='Inregistrat')
    raspuns = models.TextField(null=True, blank=True)

