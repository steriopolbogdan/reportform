from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from reports.models import Report


def home_view(request):
    context = get_common_data(request)
    return render(request, 'administration/home.html', context)


def get_common_data(request, context=None):
    if context is None:
        context = {}
    context['current_user'] = request.user

    return context


@login_required
def update_report_status(request):
    if request.method == 'POST':
        print(request.POST)
        report_id = request.POST.get('report_id', 1)
        status = request.POST.get('status', 1)
        report = Report.objects.get(id=report_id)
        if status == 'inchis':
            raspuns = request.POST.get('raspuns', '')
            report.raspuns = raspuns
        report.status = status
        report.save()

    return redirect(reverse_lazy('rapoarte'))


@login_required
def reports_list_view(request):
    reports_list = Report.objects.all()
    page = request.GET.get('page', 1)

    paginator = Paginator(reports_list, 10)
    try:
        reports = paginator.page(page)
    except PageNotAnInteger:
        reports = paginator.page(1)
    except EmptyPage:
        reports = paginator.page(paginator.num_pages)

    return render(request, 'administration/reports.html', {'reports': reports})
