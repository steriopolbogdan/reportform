from django.urls import path

from administration import views

urlpatterns = [
    path('', views.home_view, name='administration_home'),
    # path('vezi_raportari/', views.ReportListView.as_view(), name='rapoarte'),
    path('update_report_status/', views.update_report_status, name='update_report_status'),
    path('vezi_raportari/', views.reports_list_view, name='rapoarte'),
]
